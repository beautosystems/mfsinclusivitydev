package com.mfs.api.client;

import java.rmi.RemoteException;

import com.mfs.api.client.XPServiceStub.Credential;
import com.mfs.api.client.XPServiceStub.Mm_trans_log;
import com.mfs.api.client.XPServiceStub.Money;
import com.mfs.api.client.XPServiceStub.Sender;
import com.mfs.api.client.XPServiceStub.Trans_com;
import com.mfs.api.client.XPServiceStub.Trans_comResponse;

public class AppTest {

	public static void main(String[] args) throws RemoteException {
		XPServiceStub serviceStub = new XPServiceStub("https://mfsapitest.com/services/XPService");
		Mm_trans_log transLogRequest = new Mm_trans_log();
		Credential credential = new Credential();
		Money transLogMoney = new Money();
		Sender transLogsender = new Sender();
//		Mm_trans_logResponse transLogResponse = serviceStub.mm_trans_log(transLogRequest);
//		System.out.println(transLogResponse.get_return().getMfs_trans_id()); testing
		
		Trans_com transComRequest = new Trans_com();
		Trans_comResponse transComResponse = serviceStub.trans_com(transComRequest);

	}

}
