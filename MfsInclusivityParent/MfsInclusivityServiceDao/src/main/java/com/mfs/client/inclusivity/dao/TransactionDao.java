package com.mfs.client.inclusivity.dao;

import java.util.List;

import com.mfs.client.inclusivity.models.InclusivityAuthorizationModel;
import com.mfs.client.inclusivity.models.InclusivityPartnerCountry;
import com.mfs.client.inclusivity.models.InclusivityPushJobTrack;
import com.mfs.client.inclusivity.models.InclusivitySMSLogQueue;
import com.mfs.client.inclusivity.models.InclusivitySMSLogSuccess;
import com.mfs.client.inclusivity.models.InclusivityTransactionData;

public interface TransactionDao extends BaseDao {

	InclusivityPartnerCountry getPartnerDetail();

	InclusivityTransactionData getTransactionByMfstransId(String transaction_id);

	List<InclusivityTransactionData> getRetryTransactionData();

	List<InclusivitySMSLogQueue> getSmsRecord();

	List<InclusivitySMSLogSuccess> getSmsLogSuccessData();

	InclusivitySMSLogQueue deleteLogQueueDataByMsisdn(String msisdn);

	InclusivityPushJobTrack getSchedulerData();

	void deleteSmsInQueueById(int smsId);

	InclusivityAuthorizationModel getTokenData();

}
