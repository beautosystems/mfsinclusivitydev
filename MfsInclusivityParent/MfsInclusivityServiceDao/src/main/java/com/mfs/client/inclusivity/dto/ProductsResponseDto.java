package com.mfs.client.inclusivity.dto;

import java.util.List;

public class ProductsResponseDto {

	private String guid;
	private String name;
	private String type;
	private String code;
	private String cover_type;
	private int waiting_period_days;
	private List<PremiumsResponseDto> premiums;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCover_type() {
		return cover_type;
	}

	public void setCover_type(String cover_type) {
		this.cover_type = cover_type;
	}

	public int getWaiting_period_days() {
		return waiting_period_days;
	}

	public void setWaiting_period_days(int waiting_period_days) {
		this.waiting_period_days = waiting_period_days;
	}

	public List<PremiumsResponseDto> getPremiums() {
		return premiums;
	}

	public void setPremiums(List<PremiumsResponseDto> premiums) {
		this.premiums = premiums;
	}

	@Override
	public String toString() {
		return "ProductsResponseDto [guid=" + guid + ", name=" + name + ", type=" + type + ", code=" + code
				+ ", cover_type=" + cover_type + ", waiting_period_days=" + waiting_period_days + ", premiums="
				+ premiums + "]";
	}

}
