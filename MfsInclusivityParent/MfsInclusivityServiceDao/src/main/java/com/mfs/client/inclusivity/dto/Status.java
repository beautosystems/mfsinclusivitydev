package com.mfs.client.inclusivity.dto;

public class Status {
	private String groupId;
	private String groupName;
	private String id;
	private String name;
	private String description;

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Status [groupId=" + groupId + ", groupName=" + groupName + ", id=" + id + ", name=" + name
				+ ", description=" + description + "]";
	}

}
