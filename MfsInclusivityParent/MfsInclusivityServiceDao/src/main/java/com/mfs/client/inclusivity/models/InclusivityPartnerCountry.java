package com.mfs.client.inclusivity.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "icl_partner_country")
public class InclusivityPartnerCountry {
@Id
@GeneratedValue
@Column(name = "partner_id")
private int partnerId;

@Column(name = "partner_code", length = 50)
private String partnerCode;

@Column(name = "partner_name", length = 50)
private String partnerName;

@ManyToOne
@JoinColumn(name = "country_code", referencedColumnName = "country_code")
private InclusivityCountry inclusivityCountry;

@OneToOne(mappedBy = "inclusivityPartnerCountry", cascade = CascadeType.ALL)
private InclusivityPushJobTrack inclusivityPushJobTrack;

public int getPartnerId() {
return partnerId;
}

public void setPartnerId(int partnerId) {
this.partnerId = partnerId;
}

public String getPartnerCode() {
return partnerCode;
}

public void setPartnerCode(String partnerCode) {
this.partnerCode = partnerCode;
}

public String getPartnerName() {
return partnerName;
}

public void setPartnerName(String partnerName) {
this.partnerName = partnerName;
}

public InclusivityCountry getInclusivityCountry() {
return inclusivityCountry;
}

public void setInclusivityCountry(InclusivityCountry inclusivityCountry) {
this.inclusivityCountry = inclusivityCountry;
}

public InclusivityPushJobTrack getInclusivityPushJobTrack() {
return inclusivityPushJobTrack;
}

public void setInclusivityPushJobTrack(InclusivityPushJobTrack inclusivityPushJobTrack) {
this.inclusivityPushJobTrack = inclusivityPushJobTrack;
}

@Override
public String toString() {
return "InclusivityPartnerCountry [partnerId=" + partnerId + ", partnerCode=" + partnerCode + ", partnerName="
+ partnerName + ", inclusivityCountry=" + inclusivityCountry + ", inclusivityPushJobTrack="
+ inclusivityPushJobTrack + "]";
}
}