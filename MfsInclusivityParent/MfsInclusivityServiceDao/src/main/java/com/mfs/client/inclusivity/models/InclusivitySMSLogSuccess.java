package com.mfs.client.inclusivity.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "icl_sms_log_success")
public class InclusivitySMSLogSuccess {

	@Id
	@GeneratedValue
	@Column(name = "sms_id")
	private int smsId;

	@Column(name = "description", length = 100)
	private String description;

	@Column(name = "name", length = 20)
	private String name;

	@Column(name = "message_id", length = 50)
	private String messageId;

	@Column(name = "msisdn", length = 20)
	private String msisdn;

	@Column(name = "group_id", length = 20)
	private String groupId;

	@Column(name = "group_name", length = 20)
	private String groupName;

	@Column(name = "id", length = 20)
	private String id;

	@Column(name = "sms_count")
	private int smsCount;

	@Column(name = "status")
	private String status;

	public int getSmsCount() {
		return smsCount;
	}

	public void setSmsCount(int smsCount) {
		this.smsCount = smsCount;
	}

	@Column(name = "date_logged")
	private Date dateLogged;

	public int getSmsId() {
		return smsId;
	}

	public void setSmsId(int smsId) {
		this.smsId = smsId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "InclusivitySMSLogSuccess [smsId=" + smsId + ", description=" + description + ", name=" + name
				+ ", messageId=" + messageId + ", msisdn=" + msisdn + ", groupId=" + groupId + ", groupName="
				+ groupName + ", id=" + id + ", smsCount=" + smsCount + ", status=" + status + ", dateLogged="
				+ dateLogged + "]";
	}

}
