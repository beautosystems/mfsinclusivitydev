package com.mfs.client.inclusivity.models;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "icl_status_detail")
public class InclusivityStatusDetail {

	@Id
	@GeneratedValue
	@Column(name = "status_detail_id")
	private int statusDetailId;

	@Column(name = "msisdn", length = 20)
	private String msisdn;

	@Column(name = "national_id", length = 20)
	private String nationalId;

	@Column(name = "full_name", length = 510)
	private String fullName;

	@Column(name = "date_of_birth")
	private Date dateOfBirth;

	@Column(name = "service_name", length = 20)
	private String serviceName;

	@Column(name = "policies_channel", length = 20)
	private String policiesChannel;

	@Column(name = "policies_status", length = 50)
	private String policiesStatus;

	@Column(name = "is_registered")
	private boolean isRegistered;

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	@Column(name = "date_logged")
	private Date dateLogged;

	@OneToOne(mappedBy = "inclusivityStatusDetail", cascade = CascadeType.ALL)
	private InclusivityStatusLoyalities inclusivityStatusLoyalities;

	public int getStatusDetailId() {
		return statusDetailId;
	}

	public void setStatusDetailId(int statusDetailId) {
		this.statusDetailId = statusDetailId;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getNationalId() {
		return nationalId;
	}

	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getPoliciesChannel() {
		return policiesChannel;
	}

	public void setPoliciesChannel(String policiesChannel) {
		this.policiesChannel = policiesChannel;
	}

	public String getPoliciesStatus() {
		return policiesStatus;
	}

	public void setPoliciesStatus(String policiesStatus) {
		this.policiesStatus = policiesStatus;
	}

	public boolean isRegistered() {
		return isRegistered;
	}

	public void setRegistered(boolean isRegistered) {
		this.isRegistered = isRegistered;
	}

	public InclusivityStatusLoyalities getInclusivityStatusLoyalities() {
		return inclusivityStatusLoyalities;
	}

	public void setInclusivityStatusLoyalities(InclusivityStatusLoyalities inclusivityStatusLoyalities) {
		this.inclusivityStatusLoyalities = inclusivityStatusLoyalities;
	}

	@Override
	public String toString() {
		return "InclusivityStatusDetail [statusDetailId=" + statusDetailId + ", msisdn=" + msisdn + ", nationalId="
				+ nationalId + ", fullName=" + fullName + ", dateOfBirth=" + dateOfBirth + ", serviceName="
				+ serviceName + ", policiesChannel=" + policiesChannel + ", policiesStatus=" + policiesStatus
				+ ", isRegistered=" + isRegistered + ", dateLogged=" + dateLogged + ", inclusivityStatusLoyalities="
				+ inclusivityStatusLoyalities + "]";
	}

}