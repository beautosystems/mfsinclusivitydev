package com.mfs.client.inclusivity.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "icl_transaction_audit")
public class InclusivityTransactionAudit {

	@Id
	@GeneratedValue
	@Column(name = "trans_audit_id")
	private long transAuditId;

	@Column(name = "service_name", length = 20)
	private String serviceName;

	@Column(name = "status", length = 20)
	private String status;

	@Column(name = "amount_in_cents", length = 50)
	private String amountInCents;

	@Column(name = "currency", length = 20)
	private String currency;

	@Column(name = "msisdn", length = 20)
	private String msisdn;

	@Column(name = "transaction_id", length = 50)
	private String transactionId;

	@Column(name = "date_logged")
	private Date dateLogged;

	public long getTransAuditId() {
		return transAuditId;
	}

	public void setTransAuditId(long transAuditId) {
		this.transAuditId = transAuditId;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAmountInCents() {
		return amountInCents;
	}

	public void setAmountInCents(String amountInCents) {
		this.amountInCents = amountInCents;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	@Override
	public String toString() {
		return "InclusivityTransactionAudit [transAuditId=" + transAuditId + ", serviceName=" + serviceName
				+ ", status=" + status + ", amountInCents=" + amountInCents + ", currency=" + currency + ", msisdn="
				+ msisdn + ", transactionId=" + transactionId + ", dateLogged=" + dateLogged + "]";
	}

}
