package com.mfs.client.inclusivity.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "icl_transaction_data")
public class InclusivityTransactionData {



@Id
@GeneratedValue
@Column(name = "transaction_data_id")
private int transactionDataId;

@Column(name = "partner_guid", length = 20)
private String partnerGuid;

@Column(name = "mfs_trans_id", length = 100)
private String mfsTransId;

@Column(name = "amount", length = 50)
private String amount;

@Column(name = "national_id", length = 20)
private String nationalId;

@Column(name = "currency", length = 20)
private String currency;

@Column(name = "msisdn", length = 20)
private String msisdn;

@Column(name = "full_name", length = 510)
private String fullName;

@Column(name = "date_of_birth")
private String dateOfBirth;

@Column(name = "response_code", length = 20)
private String responseCode;

@Column(name = "response_message", length = 250)
private String responseMessage;

@Column(name = "date_logged")
private Date dateLogged;

@Column(name = "count")
private int count;

public int getTransactionDataId() {
return transactionDataId;
}

public void setTransactionDataId(int transactionDataId) {
this.transactionDataId = transactionDataId;
}

public String getPartnerGuid() {
return partnerGuid;
}

public void setPartnerGuid(String partnerGuid) {
this.partnerGuid = partnerGuid;
}

public String getMfsTransId() {
return mfsTransId;
}

public void setMfsTransId(String mfsTransId) {
this.mfsTransId = mfsTransId;
}

public String getAmount() {
return amount;
}

public void setAmount(String amount) {
this.amount = amount;
}

public String getNationalId() {
return nationalId;
}

public void setNationalId(String nationalId) {
this.nationalId = nationalId;
}

public String getCurrency() {
return currency;
}

public void setCurrency(String currency) {
this.currency = currency;
}

public String getMsisdn() {
return msisdn;
}

public void setMsisdn(String msisdn) {
this.msisdn = msisdn;
}

public String getFullName() {
return fullName;
}

public void setFullName(String fullName) {
this.fullName = fullName;
}

public String getDateOfBirth() {
return dateOfBirth;
}

public void setDateOfBirth(String dateOfBirth) {
this.dateOfBirth = dateOfBirth;
}

public String getResponseCode() {
return responseCode;
}

public void setResponseCode(String responseCode) {
this.responseCode = responseCode;
}

public String getResponseMessage() {
return responseMessage;
}

public void setResponseMessage(String responseMessage) {
this.responseMessage = responseMessage;
}

public Date getDateLogged() {
return dateLogged;
}

public void setDateLogged(Date dateLogged) {
this.dateLogged = dateLogged;
}

public int getCount() {
return count;
}

public void setCount(int count) {
this.count = count;
}

@Override
public String toString() {
return "InclusivityTransactionData [transactionDataId=" + transactionDataId + ", partnerGuid=" + partnerGuid
+ ", mfsTransId=" + mfsTransId + ", amount=" + amount + ", nationalId=" + nationalId + ", currency="
+ currency + ", msisdn=" + msisdn + ", fullName=" + fullName + ", dateOfBirth=" + dateOfBirth
+ ", responseCode=" + responseCode + ", responseMessage=" + responseMessage + ", dateLogged="
+ dateLogged + ", count=" + count + "]";
}


}