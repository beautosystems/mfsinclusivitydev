package com.mfs.client.inclusivity.scheduler;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.mfs.client.inclusivity.dao.InclusivitySystemConfigDao;
import com.mfs.client.inclusivity.service.NotifyService;
import com.mfs.client.inclusivity.service.PushAndRetryTransactionDataService;
import com.mfs.client.inclusivity.service.TransactionDataService;

@EnableScheduling
public class InclusivityScheduler {

	@Autowired
	TransactionDataService transactionDataService;

	@Autowired
	PushAndRetryTransactionDataService retryTransactionDataService;

	@Autowired
	NotifyService notifyService;

	@Autowired
	InclusivitySystemConfigDao inclusivitySystemConfigDao;

	/*
	 * @Autowired DeleteSmsLogQueueSerivce deleteSmsLogQueueSerivce;
	 */

	private static final Logger LOGGER = Logger.getLogger(InclusivityScheduler.class);

	@Scheduled(cron = "0 0 */2 ? * *")
	public void curbi() {

		LOGGER.info("start with curbi Transaction Data into incusivity adaptor");

		transactionDataService.getCurbiTransaction();

	}

	@Scheduled(cron = "0 0 0/2 ? * *")
	public void transactionRetryScheduler() {

		LOGGER.info("start with push and retry data from incusivity");

		retryTransactionDataService.retryTransaction();

	}

	@Scheduled(cron = "0 */5 * ? * *")
	public void sendSMS() {

		LOGGER.info("start with send sms via sms api");

		notifyService.sendSMS();

	}

}