package com.mfs.client.inclusivity.service;

import com.mfs.client.inclusivity.dto.StatusResponseDto;

public interface CustomerStatusService {
	
	public StatusResponseDto getStatus(String msisdn);

}
