package com.mfs.client.inclusivity.service;

import com.mfs.client.inclusivity.dto.ResponseStatus;
import com.mfs.client.inclusivity.dto.SMSApiRequestDto;

public interface LogSmsQueueData {

	public ResponseStatus logSmsQueueData(SMSApiRequestDto requestDto , String headerValue);

}
