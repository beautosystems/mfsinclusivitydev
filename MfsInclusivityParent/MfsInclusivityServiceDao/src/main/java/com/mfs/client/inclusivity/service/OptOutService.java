package com.mfs.client.inclusivity.service;

import com.mfs.client.inclusivity.dto.ResponseStatus;

public interface OptOutService {

	public ResponseStatus optOut(String msisdn);

}
