package com.mfs.client.inclusivity.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.commons.codec.binary.Base64;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.inclusivity.dao.InclusivitySystemConfigDao;
import com.mfs.client.inclusivity.dao.TransactionDao;
import com.mfs.client.inclusivity.dto.AuthorizationResponseDto;
import com.mfs.client.inclusivity.models.InclusivityAuthorizationModel;
import com.mfs.client.inclusivity.service.AuthorizationService;
import com.mfs.client.inclusivity.util.CallServices;
import com.mfs.client.inclusivity.util.CommonConstant;
import com.mfs.client.inclusivity.util.CommonValidations;
import com.mfs.client.inclusivity.util.HttpsConnectionRequest;
import com.mfs.client.inclusivity.util.JSONToObjectConversion;

@Service("AuthorizationService")
public class AuthorizationServiceImpl implements AuthorizationService {

	private static final Logger LOGGER = Logger.getLogger(AuthorizationServiceImpl.class);

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	InclusivitySystemConfigDao inclusivityConfig;

	@Resource(name = "responseC")
	private Properties responseCodes;

	public AuthorizationResponseDto authorizationService() {

		Map<String, String> configMap = null;
		String authResponse = null;
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		AuthorizationResponseDto authorizationResponse = new AuthorizationResponseDto();

		try {

			configMap = inclusivityConfig.getConfigDetailsMap();

			StringBuilder string = new StringBuilder();
			string.append(configMap.get(CommonConstant.AUTH_USERNAME));
			string.append(":");
			string.append(configMap.get(CommonConstant.AUTH_PASSWORD));

			byte[] authEncBytes = Base64.encodeBase64(string.toString().getBytes());
			String authStringEnc = new String(authEncBytes);

			authStringEnc = CommonConstant.BASIC + authStringEnc;

			connectionRequest.setHttpmethodName(CommonConstant.POST);
			if (CommonValidations.isStringNotEmptyAndNotNull(configMap.get(CommonConstant.PORT))) {
				connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT)));
			}
			connectionRequest.setServiceUrl(
					configMap.get(CommonConstant.INCLUSIVITY_URL) + configMap.get(CommonConstant.AUTH_ENDPOINT));

			// set Headers
			Map<String, String> headerMap = new HashMap<String, String>();
			headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
			headerMap.put(CommonConstant.AUTHORIZATION, authStringEnc);
			connectionRequest.setHeaders(headerMap);

			authResponse = CallServices.getResponseFromService(connectionRequest, null);

			if (null != authResponse) {
				authorizationResponse = (AuthorizationResponseDto) JSONToObjectConversion
						.getObjectFromJson(authResponse, AuthorizationResponseDto.class);
				LOGGER.info("AuthorizationTokenServiceImpl in createToken function response " + authorizationResponse);
				logResponse(authorizationResponse);
			} else {
				LOGGER.error("==>No auth response or auth failed" + authResponse);

			}

		} catch (Exception e) {
			LOGGER.error("==>Exception in AuthorizationServiceImpl" + e);

		}
		return authorizationResponse;
	}

	private void logResponse(AuthorizationResponseDto authorizationResponse) {

		InclusivityAuthorizationModel inclusivityAuthorizationModel = new InclusivityAuthorizationModel();
		inclusivityAuthorizationModel.setAccessToken(authorizationResponse.getAccess_token());
		inclusivityAuthorizationModel.setTokenType(authorizationResponse.getToken_type());
		inclusivityAuthorizationModel.setExpiresIn(authorizationResponse.getExpires_in());
		inclusivityAuthorizationModel.setScope(authorizationResponse.getScope());
		inclusivityAuthorizationModel.setUserClientName(authorizationResponse.getUser_client_name());
		inclusivityAuthorizationModel.setUserPrinciple(authorizationResponse.getUser_principle());
		inclusivityAuthorizationModel.setDateLogged(new Date());
		transactionDao.save(inclusivityAuthorizationModel);

	}

}