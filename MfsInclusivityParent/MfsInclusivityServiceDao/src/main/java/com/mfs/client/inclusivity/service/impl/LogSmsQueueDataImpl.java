package com.mfs.client.inclusivity.service.impl;

import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.inclusivity.dao.InclusivitySystemConfigDao;
import com.mfs.client.inclusivity.dao.TransactionDao;
import com.mfs.client.inclusivity.dto.ResponseStatus;
import com.mfs.client.inclusivity.dto.SMSApiRequestDto;
import com.mfs.client.inclusivity.models.InclusivitySMSLogQueue;
import com.mfs.client.inclusivity.service.LogSmsQueueData;
import com.mfs.client.inclusivity.util.CommonConstant;
import com.mfs.client.inclusivity.util.SHA256Conversion;

@Service
public class LogSmsQueueDataImpl implements LogSmsQueueData {

	private static final Logger LOGGER = Logger.getLogger(LogSmsQueueDataImpl.class);

	@Autowired
	TransactionDao transactionDao;

	@Resource(name = "responseC")
	private Properties responseCodes;

	@Autowired
	InclusivitySystemConfigDao inclusivitySystemConfigDao;

	public ResponseStatus logSmsQueueData(SMSApiRequestDto requestDto, String headerValue) {

		ResponseStatus responseStatus = new ResponseStatus();
		String hashKey = null;
		Map<String, String> configMap = null;

		// getting data from config table
		configMap = inclusivitySystemConfigDao.getConfigDetailsMap();

		String input = requestDto.getMsisdn() + requestDto.getDateTime() + configMap.get(CommonConstant.PRIVATE_KEY);

		LOGGER.info("==> kEY BEFORE GENERATING SHA256 :: " + input);
		System.out.println("==> kEY BEFORE GENERATING SHA256 :: " + input);
		
		try {
			byte[] key = SHA256Conversion.getSHA(input);
			hashKey = SHA256Conversion.toHexString(key);

			LOGGER.info("==> kEY AFTER GENERATING SHA256 :: " + hashKey);
			System.out.println("==> kEY AFTER GENERATING SHA256 :: " + hashKey);
		} catch (NoSuchAlgorithmException e) {

			LOGGER.error("==>Exception in LogSmsQueueDataImpl" + e);
		}

		if (headerValue.equals(hashKey)) {

			InclusivitySMSLogQueue inclusivitySMSLogQueue = new InclusivitySMSLogQueue();
			inclusivitySMSLogQueue.setMsisdn(requestDto.getMsisdn());
			inclusivitySMSLogQueue.setMessage(requestDto.getMessage());
			inclusivitySMSLogQueue.setDateTime(requestDto.getDateTime());
			inclusivitySMSLogQueue.setDateLogged(new Date());
			boolean result = transactionDao.save(inclusivitySMSLogQueue);
			if (result) {
				responseStatus.setStatusCode(CommonConstant.SUCCESS_CODE);
				responseStatus.setStatusMessage(responseCodes.getProperty(CommonConstant.SUCCESS_CODE));
			} else {
				responseStatus.setStatusCode(CommonConstant.FAIL_CODE);
				responseStatus.setStatusMessage(responseCodes.getProperty(CommonConstant.FAIL_CODE));
			}
		} else {

			responseStatus.setStatusCode(CommonConstant.INVALID_AUTHOURIZATION);
			responseStatus.setStatusMessage(responseCodes.getProperty(CommonConstant.INVALID_AUTHOURIZATION));

		}
		return responseStatus;

	}
}
