package com.mfs.client.inclusivity.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.client.inclusivity.dao.InclusivitySystemConfigDao;
import com.mfs.client.inclusivity.dao.TransactionDao;
import com.mfs.client.inclusivity.dto.AuthorizationResponseDto;
import com.mfs.client.inclusivity.dto.TransactionDataDto;
import com.mfs.client.inclusivity.models.InclusivityAuthorizationModel;
import com.mfs.client.inclusivity.models.InclusivityTransactionData;
import com.mfs.client.inclusivity.service.AuthorizationService;
import com.mfs.client.inclusivity.service.PushAndRetryTransactionDataService;
import com.mfs.client.inclusivity.util.CallServices;
import com.mfs.client.inclusivity.util.CommonConstant;
import com.mfs.client.inclusivity.util.CommonValidations;
import com.mfs.client.inclusivity.util.ExpiredToken;
import com.mfs.client.inclusivity.util.HttpsConnectionRequest;

@Service("PushAndRetryTransactionDataService")
public class PushAndRetryTransactionDataServiceImpl implements PushAndRetryTransactionDataService {

	private static final Logger LOGGER = Logger.getLogger(PushAndRetryTransactionDataServiceImpl.class);

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	InclusivitySystemConfigDao inclusivitySystemConfigDao;

	@Autowired
	AuthorizationService authorizationService;

	@Resource(name = "responseC")
	private Properties responseCodes;

	public void retryTransaction() {

		Map<String, String> configMap = null;
		int retryCount = 0;

		String inclusivityResponse = null;
		AuthorizationResponseDto response = null;
		String token = null;

		try {

			InclusivityAuthorizationModel inclusivityAuthorizationModel = transactionDao.getTokenData();

			if (null != inclusivityAuthorizationModel && ExpiredToken.isTokenExpire(inclusivityAuthorizationModel)) {

				token = inclusivityAuthorizationModel.getAccessToken();
			} else {
				response = new AuthorizationResponseDto();
				response = authorizationService.authorizationService();
				if (null != response) {
// authorizationService.logResponse(response);
					token = response.getAccess_token();
				}

				else {
					LOGGER.error("==>Null auth response " + response);

				}

			}
			List<InclusivityTransactionData> inclusivityTransactionData = transactionDao.getRetryTransactionData();

			if (null != inclusivityTransactionData && !(inclusivityTransactionData.isEmpty())) {

				configMap = inclusivitySystemConfigDao.getConfigDetailsMap();

				Gson gson = new Gson();
				HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
				String partnerGuId = configMap.get(CommonConstant.PARTNER_GUID);
				String nationalId = configMap.get(CommonConstant.NATIONAL_ID);
				retryCount = Integer.parseInt(configMap.get(CommonConstant.RETRY_COUNT));
				Map<String, String> headerMap = new HashMap<String, String>();
				headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);

				connectionRequest.setHeaders(headerMap);
				connectionRequest.setHttpmethodName(CommonConstant.POST);
				connectionRequest.setServiceUrl(configMap.get(CommonConstant.INCLUSIVITY_URL)
						+ configMap.get(CommonConstant.REMITANCE_ENDPOINT) + CommonConstant.ACCESS_TOKEN + token);

				if (CommonValidations.isStringNotEmptyAndNotNull(configMap.get(CommonConstant.PORT))) {
					connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT)));
				}

				for (InclusivityTransactionData transactionDataDto1 : inclusivityTransactionData) {

					if (transactionDataDto1.getCount() <= retryCount) {

						BigDecimal dollars = new BigDecimal(transactionDataDto1.getAmount());

						long cents = dollars.multiply(new BigDecimal(100)).intValue();
						TransactionDataDto transactionDataDto = new TransactionDataDto();
						transactionDataDto.setPartner_guid(partnerGuId);
						transactionDataDto.setNational_id(nationalId);
						transactionDataDto.setCurrency(transactionDataDto1.getCurrency());
						transactionDataDto.setDate_of_birth(transactionDataDto1.getDateOfBirth());
						transactionDataDto.setMsisdn("00" + transactionDataDto1.getMsisdn());

						transactionDataDto.setAmount_in_cents(cents + "");
						transactionDataDto.setTransaction_id(transactionDataDto1.getMfsTransId());
						transactionDataDto.setFull_name(transactionDataDto1.getFullName());

						inclusivityResponse = CallServices.getResponseFromService(connectionRequest,
								gson.toJson(transactionDataDto));

						if (CommonValidations.isStringNotEmptyAndNotNull(inclusivityResponse)) {

							if (inclusivityResponse.equals(CommonConstant.ICL_SUCCESS_CODE)) {
								transactionDao.delete(transactionDataDto1);
							} else if (inclusivityResponse.equals(CommonConstant.ICL_FAIL_CODE)
									|| (inclusivityResponse.charAt(0)) == '5') {
								logInclusivityResponseData(inclusivityResponse, transactionDataDto1,
										transactionDataDto1.getCount(), partnerGuId, nationalId);
							} else if (inclusivityResponse.equals(CommonConstant.TOKEN_EXPIRY_CODE)) {

								response = new AuthorizationResponseDto();
								response = authorizationService.authorizationService();

//     authorizationService.logResponse(response);

								callIclAgain(connectionRequest, transactionDataDto, response.getAccess_token(),
										partnerGuId, nationalId, transactionDataDto1.getCount(), transactionDataDto1);

							} else {
								LOGGER.error("==>different error code not in requirement " + inclusivityResponse);
							}
						}

						else {
							LOGGER.error("==>null response from inclusivity " + inclusivityResponse);
						}

					} else {
						LOGGER.error("==>count limit of retry is over " + transactionDataDto1.getCount());

					}
				}

			} else {
				LOGGER.error("No data to call retry" + inclusivityTransactionData);
			}

		} catch (

		Exception e) {

			LOGGER.error("==>Exception in RetryTransactionDataServiceImpl" + e);
		}

	}

	private void callIclAgain(HttpsConnectionRequest connectionRequest, TransactionDataDto transactionDataDto,
			String accessToken, String partnerGuId, String nationalId, int retryCount,
			InclusivityTransactionData transactionDataDto1) {

		Gson gson = new Gson();

		String inclusivityResponse = CallServices.getResponseFromService(connectionRequest,
				gson.toJson(transactionDataDto));

		if (CommonValidations.isStringNotEmptyAndNotNull(inclusivityResponse)) {

			if (inclusivityResponse.equals(CommonConstant.ICL_SUCCESS_CODE)) {
				transactionDao.delete(transactionDataDto);
			} else if (inclusivityResponse.equals(CommonConstant.ICL_FAIL_CODE)
					|| (inclusivityResponse.charAt(0)) == CommonConstant.FAIL_ERROR_CHECK) {
				logInclusivityResponseData(inclusivityResponse, transactionDataDto1, retryCount, partnerGuId,
						nationalId);
			}
		}
	}

	private void logInclusivityResponseData(String response, InclusivityTransactionData transactionDataDto1, int count,
			String partnerGuId, String nationalId) {
		transactionDataDto1.setDateLogged(new Date());
		transactionDataDto1.setResponseCode(response);
		if (response.charAt(0) == CommonConstant.FAIL_ERROR_CHECK) {

			transactionDataDto1.setCount(count + 1);
			transactionDataDto1.setResponseMessage(CommonConstant.RESPONSEMESSAGE2);
		} else {
			transactionDataDto1.setResponseMessage(CommonConstant.RESPONSEMESSAGE1);
		}
		transactionDataDto1.setPartnerGuid(partnerGuId);
		transactionDataDto1.setNationalId(nationalId);
		transactionDao.update(transactionDataDto1);
	}

}