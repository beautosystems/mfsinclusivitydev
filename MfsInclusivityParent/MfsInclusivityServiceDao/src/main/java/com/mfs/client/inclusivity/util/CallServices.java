package com.mfs.client.inclusivity.util;

import java.util.HashMap;
import java.util.Map;

public class CallServices {

	public static String getResponseFromService(HttpsConnectionRequest connectionRequest, String request) {

		HttpsConnectionResponse httpsConResult = null;
		String str_result = null;

		try {

			// boolean isHttps = true;
			if (connectionRequest.getServiceUrl().startsWith("https")) {

				connectionRequest.setPort(connectionRequest.getPort());
				HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();
				httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(request, connectionRequest);
			} else {

				HttpConnectorImpl httpConnectorImpl = new HttpConnectorImpl();
				httpsConResult = httpConnectorImpl.httpUrlConnection(connectionRequest, request);
			}

			if (httpsConResult.getResponseData() == null) {
				str_result = httpsConResult.getRespCode() + "";
			} else {
				str_result = httpsConResult.getResponseData();
			}
		} catch (

		Exception e) {

		}
		return str_result;
	}

	public static String getResponseFromService1(String url, String signature) throws Exception {

		HttpsConnectionResponse httpsConResult = null;
		String str_result = null;

		try {
			HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();

			Map<String, String> headerMap = new HashMap<String, String>();

			headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.TEXT_XML);
			// headerMap.put(CommonConstant.AUTHORIZATION, signature);

			connectionRequest.setServiceUrl(url);
			connectionRequest.setHeaders(headerMap);
			connectionRequest.setHttpmethodName("POST");

			HttpConnectorImpl httpConnectorImpl = new HttpConnectorImpl();
			httpsConResult = httpConnectorImpl.httpUrlConnection(connectionRequest, signature);
			str_result = httpsConResult.getResponseData();

		} catch (Exception e) {
			throw new Exception(e);
		}
		return str_result;
	}

}
