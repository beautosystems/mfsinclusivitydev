package com.mfs.client.inclusivity.util;

import java.util.Map;

public class ConnectionRequest
{
  private String serviceUrl;
  private String httpmethodName;
  private int port;
  private boolean doOutPut;
  private Map<String, String> headers;
  private String username;
  private String password;
  private String httpmethodType;
  
  public String getServiceUrl()
  {
    return this.serviceUrl;
  }
  
  public void setServiceUrl(String paramString)
  {
    this.serviceUrl = paramString;
  }
  
  public String getHttpmethodName()
  {
    return this.httpmethodName;
  }
  
  public void setHttpmethodName(String paramString)
  {
    this.httpmethodName = paramString;
  }
  
  public int getPort()
  {
    return this.port;
  }
  
  public void setPort(int paramInt)
  {
    this.port = paramInt;
  }
  
  public boolean isDoOutPut()
  {
    return this.doOutPut;
  }
  
  public void setDoOutPut(boolean paramBoolean)
  {
    this.doOutPut = paramBoolean;
  }
  
  public Map<String, String> getHeaders()
  {
    return this.headers;
  }
  
  public void setHeaders(Map<String, String> paramMap)
  {
    this.headers = paramMap;
  }
  
  public String getUsername()
  {
    return this.username;
  }
  
  public void setUsername(String paramString)
  {
    this.username = paramString;
  }
  
  public String getPassword()
  {
    return this.password;
  }
  
  public void setPassword(String paramString)
  {
    this.password = paramString;
  }
  
  public String getHttpmethodType()
  {
    return this.httpmethodType;
  }
  
  public void setHttpmethodType(String paramString)
  {
    this.httpmethodType = paramString;
  }
}


