package com.mfs.client.inclusivity.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateTimeDifference {

	public static String startDate(Timestamp endDate) {

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String strDate = dateFormat.format(endDate);

//		System.out.println("Before subtraction of hours from date: " + strDate);

		LocalDateTime datetime = LocalDateTime.parse(strDate, formatter);

		datetime = datetime.minusHours(24);

		String startDate = datetime.format(formatter);
	//	System.out.println("After 24 hours subtraction from date: " + startDate);

		return startDate;

		
	}

}
