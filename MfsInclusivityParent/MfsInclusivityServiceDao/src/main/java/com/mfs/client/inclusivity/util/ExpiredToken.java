package com.mfs.client.inclusivity.util;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.mfs.client.inclusivity.models.InclusivityAuthorizationModel;

public class ExpiredToken {

	public static boolean isTokenExpire(InclusivityAuthorizationModel inclusivityAuthorizationModel) {
		long diffTime = 0;

		Calendar start = Calendar.getInstance();
		Calendar end = Calendar.getInstance();

		start.setTime(inclusivityAuthorizationModel.getDateLogged());
		end.setTime(new Date());

		Date startDate = start.getTime();
		Date endDate = end.getTime();

		long startTime = startDate.getTime();
		long endTime = endDate.getTime();

		diffTime = endTime - startTime;
		long seconds1 = TimeUnit.MILLISECONDS.toSeconds(diffTime);

		int seconds = Integer.parseInt(inclusivityAuthorizationModel.getExpiresIn());

		if (seconds1 <= seconds) {
			return true;
		}

		return false;
	}

}
