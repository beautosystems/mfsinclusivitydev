package com.mfs.client.inclusivity.util;

import org.apache.log4j.Logger;

public class TrustAllTrustManager implements javax.net.ssl.X509TrustManager {

	private static final Logger LOGGER = Logger.getLogger(TrustAllTrustManager.class);

	public java.security.cert.X509Certificate[] getAcceptedIssuers() {

		return new java.security.cert.X509Certificate[] {};

	}

	public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
		LOGGER.debug("Start checkClientTrusted() of TrustAllTrustManager");
		LOGGER.debug("trust certificate" + certs);
		LOGGER.debug("auth type" + authType);
		LOGGER.debug("End checkClientTrusted() of TrustAllTrustManager");
	}

	public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
		LOGGER.debug("Start checkServerTrusted() of TrustAllTrustManager");
		for (int index = 0; certs != null && index < certs.length; index++) {
			LOGGER.debug("Certificate [" + index + "]: " + certs[0].getSubjectDN().getName());
		}
		LOGGER.debug("auth type" + authType);
		LOGGER.debug("End checkServerTrusted() of TrustAllTrustManager");
	}

}
