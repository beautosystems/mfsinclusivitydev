package com.mfs.client.inclusivity.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.inclusivity.dto.ResponseStatus;
import com.mfs.client.inclusivity.service.ClaimService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class ClaimServiceTest {

	@Autowired
	ClaimService claimService;

	// Function to test success
	@Ignore
	@Test
	public void claimServiceSuccessTest() {

		ResponseStatus response = claimService.claim("00250002501234");
		Assert.assertEquals("202", response.getStatusCode());
		System.out.println(response);
	}

	// Function to test Not Null
	@Ignore
	@Test
	public void claimServiceNotNullTest() {

		ResponseStatus response = claimService.claim("00250002501234");
		Assert.assertNotNull(response);
		System.out.println(response);
	}

	// Function to test Fail
	@Ignore
	@Test
	public void claimServiceFailTest() {

		ResponseStatus response = claimService.claim("250786131337");
		Assert.assertEquals("404", response.getStatusCode());
		System.out.println(response);

	}

}