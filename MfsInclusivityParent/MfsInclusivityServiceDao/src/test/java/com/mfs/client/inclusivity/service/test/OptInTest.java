package com.mfs.client.inclusivity.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.inclusivity.dto.ResponseStatus;
import com.mfs.client.inclusivity.service.OptInService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class OptInTest {

	@Autowired
	OptInService optInService;

	// function to test success
	@Ignore
	@Test
	public void optInServiceSuccessTest() {

		ResponseStatus response = optInService.optIn("00250002501234");
		Assert.assertEquals("202", response.getStatusCode());
	}

	// Function to Fail
	@Ignore
	@Test
	public void optInServiceFailTest() {

		ResponseStatus response = optInService.optIn("250002501234");
		Assert.assertEquals("400", response.getStatusCode());

	}

	// Function to NotNull
	// @Ignore
	@Test
	public void optInServiceNotNullServiceTest() {

		ResponseStatus response = optInService.optIn("00250002501234");
		Assert.assertNotNull(response);
		System.out.println(response);

	}

}
