package com.mfs.client.inclusivity.service.test;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.inclusivity.service.PushAndRetryTransactionDataService;
import com.mfs.client.inclusivity.service.TransactionDataService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class TransactionDataTest {

	@Autowired
	TransactionDataService transactionDataService;

	@Autowired
	PushAndRetryTransactionDataService retryTransactionDataService;

	@Ignore
	@Test
	public void checkforCurbiResponseSuccessTest() {
		transactionDataService.getCurbiTransaction();

	}

	@Ignore
	@Test
	public void checkforRetryTest() {
		retryTransactionDataService.retryTransaction();

	}
}
