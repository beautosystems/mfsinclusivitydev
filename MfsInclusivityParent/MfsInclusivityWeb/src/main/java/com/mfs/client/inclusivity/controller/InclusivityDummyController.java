package com.mfs.client.inclusivity.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mfs.client.inclusivity.dto.Message;
import com.mfs.client.inclusivity.dto.SendSms;
import com.mfs.client.inclusivity.dto.SmsResponse;
import com.mfs.client.inclusivity.dto.Status;
import com.mfs.client.inclusivity.dto.TransactionDataDto;

@RestController
public class InclusivityDummyController {

	@RequestMapping(value = "/transactionsuccess")
	public String dummyTransactionDataSuccess(@RequestBody TransactionDataDto transactionDataDto) {

		if (transactionDataDto.getTransaction_id().equals("1259357553606")
				|| transactionDataDto.getTransaction_id().equals("1563708554385")) {
			return "400";

		}
		if (transactionDataDto.getTransaction_id().equals("1259358585092")
				|| transactionDataDto.getTransaction_id().equals("1159358668506")) {
			return "501";
		}

		return "202";
	}

	@RequestMapping(value = "/transactionlog")
	public String dummyTransactionDataLog(String request) {

		return "400";
	}

	@RequestMapping(value = "/transactionRetrieve")
	public String dummyTransactionDataLogRetrieve(String request) {

		return "500";

	}

	@RequestMapping(value = "/smsapiresponse")
	public SmsResponse smsApiResponse(@RequestBody SendSms sendsms) {

		SmsResponse smsResponse = null;
		
			smsResponse = new SmsResponse();
			Status status = new Status();
			Message message = new Message();
			List<Message> messages = new ArrayList<Message>();
			status.setDescription("Test Success");
			status.setGroupId("22");
			status.setGroupName("ABC");
			status.setId("1");
			status.setName("Success");
			message.setSmsCount("1");
			message.setMessageId("1234567");
			message.setStatus(status);
			message.setTo("1294949392");
			messages.add(message);
			smsResponse.setMessages(messages);

		
		return smsResponse;
	}

}
